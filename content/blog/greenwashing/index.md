---
title: "GREENWASHING – A prática por trás da adesão de produtos de empresas pelos consumidores em potencial."
date: 2021-11-15T00:00:00.000Z
featuredImage: './featured.jpg'
---

<!-- wp:image {"id":2261,"width":840,"height":551,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://seivajr.com/wp-content/uploads/2021/11/Capturar-1.jpg" alt="" class="wp-image-2261" width="840" height="551"/><figcaption>Fonte: Portal Mude, 2021</figcaption></figure>


<p><span style="font-weight: 400;">Preste bastante atenção na imagem acima, quero que você tenha em mente que a ilustração de fundo (plantação de capim) representa a sustentabilidade, o ser ecológico, a mão humana representa uma empresa, corporação ou instituição, o papel como um todo sem o recorte será um produto 100% sustentável, ecologicamente produzido.</span></p>
<p><span style="font-weight: 400;">Agora o mais importante, o que levará adiante toda a problemática que será abordada, o recorte mostrando o que seria a edificação de uma indústria, retrata o real produto que uma empresa coloca no mercado quando há a prática de greenwashing, um fragmento, uma maquiagem, uma lavagem verde em cima do que seria realmente um item de compra sustentável.</span></p>
<p><span style="font-weight: 400;">Segundo o site eCycle a definição do que viria a ser greenwashing é relativamente simples. Ele pode ser praticado por empresas e indústrias públicas ou privadas, organizações não governamentais (ONGs), governos ou políticos. Consiste na estratégia de promover discursos, anúncios, ações, documentos, propagandas e campanhas publicitárias sobre ser ambientalmente correto, sustentável, verde, eco-friendly, no entanto na práxis não ser.</span></p>
<p><span style="font-weight: 400;">O termo é datado dos anos 70, possivelmente 60, sendo uma propaganda enganosa, que se arquiteta em técnicas de manipulação para os consumidores previamente selecionados.</span></p>
<p> </p>


<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img src="https://media.giphy.com/media/noMbbfnoVEB75qppGU/giphy.gif" alt="" width="434" height="433"/><figcaption>Decepcionante, né?</figcaption></figure></div>


<p></p>


<p>Quando se fala desse termo é importante conhecer os sete sinais que fundam essa prática e identificá-los.</p>


<p>1° sinal: <strong>Sem provas</strong></p>


<p>Apelar para informações ambientais, que não podem ser verificadas pelo consumidor sobre sua veracidade, não há fontes ou como pesquisar. Exemplo: certificados de sustentabilidade, veganos, orgânicos, entre outros.</p>


<p>2° sinal: <strong>Troca oculta</strong></p>


<p>Quando se tenta exemplificar o uso de um produto pela sua relevância ambiental de um aspecto e mascarando os outros que não são tão sustentáveis assim. Exemplo: sacolinhas de plástico que são dadas como recicláveis mostrando na sua própria estrutura essa informação, mas que não há a devida reciclagem e acabam tendo o mesmo destino que outras sacolinhas quaisquer, assim como o caso da Nestlé de trocar o canudo de plástico pelo de papel, só que ainda continuar embalando-os em plástico.</p>


<p>3° sinal: <strong>Vagueza e imprecisão</strong></p>


<p>Quando são usados termos que remetem a sustentabilidade, porém sem muita fundação e comprobabilidade. Exemplo: “sustentável”, “ecológico”, “amigo da natureza e meio ambiente”, dentre tantos outros.</p>


<p>4° sinal: <strong>Irrelevância</strong></p>


<p>Quando recorre a temas que podem até serem verídicos, mas é inconveniente, ou é apenas uma obrigação. Exemplo: a declaração de que a composição é livre de CFC, isso não torna o produto mais preferível em relação a outros no mercado, já que não conter esse composto é previsto na lei, qualquer empresa que incluir o ingrediente na sua base de formulação deverá ser punida.</p>


<p>5° sinal: <strong>Menor dos males</strong></p>


<p>É conhecido como uma distração, faz o consumidor focar em uma pequena “vantagem” ambiental ao consumir aquele produto, no entanto esconde outros problemas que ele ainda pode causar no meio ambiente. Exemplo: um material que é descartável e que possui menos plástico, contudo, contribui para exacerbada produção de lixo, carne e derivados orgânicos que apesar de não terem uso de aditivos e contaminantes ainda financiam a exploração animal e especismo.</p>


<p>6° sinal: <strong>Lorota</strong></p>


<p>É nitidamente quando se emprega uma mentira, a empresa diz algo para causar uma boa impressão de consciência ambiental, todavia aquela informação é falsa. Exemplo: comunicar que para aquele produto há uma coleta seletiva, entretanto não se há registro dessa atribuição ou a execução dela, ou colocar no rótulo ingredientes que na realidade não existem na composição.</p>


<p>7° sinal: <strong>Adorando falsos rótulos</strong></p>


<p>Inclui o uso de certificados (geralmente sem validade), elementos, símbolos, imagens e cores que sugerem o ecológico somente para atrair a atenção e admiração do público que se vê enganado e favorecer a compra. Exemplo: uso da cor verde, elementos como folhas, plantas, certificações de reciclável, orgânico, vegano.</p>


<p></p>


<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img src="https://i0.wp.com/assets.propmark.com.br/uploads/2021/06/home-17.png?fit=604%2C708&amp;ssl=1" alt="" width="458" height="537"/><figcaption>Tá passada?</figcaption></figure></div>


<p></p>


<p>Isso mesmo, eu tô chocada com o tanto de coisa que envolve esse sistema.</p>


<p></p>


<p>Como evitar cair no greenwashing?</p>


<p>- <strong>Pesquisar: </strong>pesquise sobre as empresas antes de consumir delas, não se conforme com o que elas dizem sobre seus produtos, investigue o histórico, as práticas e sua linha de produção, ser sustentável não é só um recorte e sim todo o processo da confecção de um produto, desde os materiais usados para a fabricação ou uso da matéria-prima até o seu tratamento de descarte.</p>


<p></p>


<div class="wp-block-image"><figure class="aligncenter size-large"><img src="https://media.giphy.com/media/Tf4gTYkbGfTjWZawYY/giphy.gif" alt=""/><figcaption>Pesquisa minha gente</figcaption></figure></div>


<p></p>


<p>- <strong>Checar veracidade: </strong>verifique se cada informação presente no rótulo é verdadeira, principalmente as certificações, elas devem ter validade.</p>


<p>- <strong>Desconfie: </strong>Pode parecer chato no início, mas a desconfiança é uma arma poderosa para não ser caloteado e subsidiar o crescimento de organizações que não se importam realmente com o meio ambiente.</p>


<p>Empresas grandes que praticam o greenwashing tendem a visar somente o crescimento econômico, mais compras, lucro, não se importando com as consequências, isso influencia a pequenas empresas agirem da mesma forma simplesmente devido a concorrência que é instalada, indo contra os seus princípios, enfim a lógica capitalista.</p>


<p></p>


<div class="wp-block-image"><figure class="aligncenter size-large"><img src="https://media.giphy.com/media/AgWQwLTByaABsBQ9Zf/giphy.gif" alt=""/><figcaption>A desconfiança é uma dádiva</figcaption></figure></div>


<p>Isso entra em questão, o consumismo e o consumo consciente, produtos ecologicamente corretos são mais caros, devido todos os cuidados que são tomados em sua produção, empresas que vestem essa imagem mesmo sendo mentira empregam esse mesmo preço, só que na realidade estamos pagando em dobro os malefícios, porque um produto ecológico, custando caro será melhor aproveitado, durável e com um descarte previsível. Já um produto somente maquiado de ecológico, será caro também, menos durável e na sua maioria das vezes sem tratamento de descarte.</p>


<p>A CONAR (Conselho Nacional de Autorregulamentação Publicitária) e o IDEC (Instituto Brasileiro de Defesa do Consumidor) são os principais responsáveis por ficarem de olho em condutas duvidosas, denúncias e manter a integridade e segurança do consumidor em meio a esses padrões.</p>


<p>Tenham em mente que ações individuais favorecem mudanças gigantescas a nível nacional e mundial. Exijam sempre mais, levem suas ideias, discursos para o trabalho (empresa onde trabalha), academia de estudos, para a sociedade, é uma troca, um ciclo.</p>


<p>Muitas das vezes nós somos um dos principais mantedores dessa prática, ao se isentar da nossa responsabilidade e possibilidade de mudar o mundo, agimos passivamente com ela, devemos nos posicionar, questionar, boicotar e denunciar quando identificarmos essas atividades criminosas e claro exigir que mais leis afins sejam implementadas. Achar que um artigo de venda é sustentável não fundamenta que ele seja. Agora é o momento de evolução, regeneração.</p>


<p></p>


<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img src="https://media.giphy.com/media/eM0zUzVl65ycKrPe1j/giphy.gif" alt="" width="416" height="416"/></figure></div>


<p></p>


<p>Vem ser ativo e sustentável com a gente!</p>


<p>Nós da Seiva Jr. temos um bate-papo sobre o greenwashing em formato de podcast, adoraríamos que vocês fossem conferir para ficarem mais antenades na temática. Vem nos ouvir!</p>


<figure class="wp-block-embed is-type-rich is-provider-spotify wp-block-embed-spotify wp-embed-aspect-21-9 wp-has-aspect-ratio"><div class="wp-block-embed__wrapper">
https://open.spotify.com/episode/5dk4SqPEk1LqIPlKzZmnrQ?si=7Wl1j7mIQWK7UCZiAhhFBg
</div><figcaption>Podcast incrível! 30 minutinhos bem investidos.</figcaption></figure>


<p>Referências:</p>


<p><a href="https://www.ecycle.com.br/greenwashing/">https://www.ecycle.com.br/greenwashing/</a></p>


<p><a href="https://neilpatel.com/br/blog/greenwa...">https://neilpatel.com/br/blog/greenwa...</a></p>


<p><a href="https://idec.org.br/greenwashing">https://idec.org.br/greenwashing</a></p>


<p>www.revista.unisal.br/lo/index.php/direitoepaz/article/download/391/269/</p>

